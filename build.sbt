enablePlugins(JavaAppPackaging, UniversalDeployPlugin, DockerPlugin, WindowsPlugin)

name := """datadigest-class-attendance-app"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

val playSlickVersion = "1.1.1"
val postgresVersion = "9.3-1103-jdbc41"

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

libraryDependencies ++= Seq(
  cache,
  ws,
  "org.webjars" %% "webjars-play" % "2.4.0-2",
  "org.webjars" %	"bootstrap" % "3.3.6",
  "org.webjars" % "bootswatch-paper" % "3.3.5+4",
//  "org.webjars" % "datatables-plugins" % "1.10.12",
  "org.webjars" % "html5shiv" % "3.7.0",
  "org.webjars" % "respond" % "1.4.2",
  "com.typesafe.play" %% "play-slick" % playSlickVersion,
  "com.typesafe.play" %% "play-slick-evolutions" % playSlickVersion,
  //  "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
  "org.postgresql" % "postgresql" % postgresVersion,
  "jp.t2v" %% "play2-auth" % "0.14.2",
  "jp.t2v" %% "play2-auth-social" % "0.14.2",
  "jp.t2v" %% "play2-auth-test" % "0.14.2" % "test",
  "org.mindrot" % "jbcrypt" % "0.3m",
  specs2 % Test,
  "com.h2database" % "h2" % "1.4.192"
)

instrumentSettings

ScoverageKeys.minimumCoverage := 70

ScoverageKeys.failOnMinimumCoverage := false

ScoverageKeys.highlighting := false

mappings in (Windows, packageDoc) := Seq()

mappings in Universal ++=
  (baseDirectory.value / "scripts" * "*" get) map
    (x => x -> ("./" + x.getName))

doc in Compile <<= target.map(_ / "none")

publishArtifact in Test := false

parallelExecution in Test := false

routesGenerator := InjectedRoutesGenerator

maintainer := "Roxane Adams <roxy.c.adams@gmail.com>"

dockerBaseImage := "anapsix/alpine-java:jre8"

dockerExposedPorts := Seq(9000)

daemonUser in Docker := "root"

// general package information (can be scoped to Windows)
packageSummary in Windows := "datadigest-class-attendance-app-windows"
packageDescription in Windows := """DataDIGEST - Class Register & Attendance App Windows MSI."""

version in Windows := "0.0.1.0"

// wix build information
wixProductId := java.util.UUID.randomUUID().toString
wixProductUpgradeId := java.util.UUID.randomUUID().toString