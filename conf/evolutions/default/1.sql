# --- !Ups
CREATE TABLE "class" (
    id SERIAL PRIMARY KEY,
    class_name TEXT NOT NULL,
    grade TEXT NOT NULL
);

CREATE TABLE "student" (
     id SERIAL PRIMARY KEY,
     first_name TEXT NOT NULL,
     last_name TEXT NOT NULL
);

CREATE TABLE "class_student" (
    id SERIAL PRIMARY KEY,
    class_id BIGINT REFERENCES "class"(id),
    student_id BIGINT REFERENCES "student"(id)
);

CREATE TABLE "attendance_record" (
    id SERIAL PRIMARY KEY,
    datetime TIMESTAMP NOT NULL,
    present BOOLEAN NOT NULL,
    class_id BIGINT REFERENCES "class"(id),
    student_id BIGINT REFERENCES "student"(id)
);

# --- !Downs
DROP TABLE "class_student";
DROP TABLE "student";
DROP TABLE "class";
DROP TABLE "attendance_record";