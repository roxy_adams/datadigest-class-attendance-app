package services

import models._

import scala.concurrent.Future

object ClazzStudentService {

  def listAllClassStudents: Future[Seq[ClazzStudent]] = {
    ClazzStudents.listAll
  }

  def listStudentsForClazz(classId: Long): Future[Seq[Student]] = {
    ClazzStudents.listStudentsForClazz(classId)
  }

  def addStudent(classId: Long, newStudent: Student): Student = {
    ClazzStudents.addStudent(classId, newStudent)
  }

}
