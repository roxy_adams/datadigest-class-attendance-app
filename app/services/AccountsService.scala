package services

import models.{Account, Accounts}

import scala.concurrent.Future

object AccountsService {

  def addUser(account: Account): Future[String] = {
    Accounts.create(account)
  }

  def listAllAccounts: Future[Seq[Account]] = {
    Accounts.listAll
  }
}
