package services

import models._

import scala.concurrent.Future

object StudentService {

  def addStudent(student: Student): Future[Student] = {
    Students.add(student)
  }

  def deleteStudent(id: Long): Future[Int] = {
    Students.delete(id)
  }

  def getStudent(id: Long): Future[Option[Student]] = {
    Students.get(id)
  }

  def listAllStudents: Future[Seq[Student]] = {
    Students.listAll
  }

  def findAllByIds(studentIds: Seq[Long]): Future[Seq[Student]] = {
    Students.findAllByIds(studentIds)
  }

}
