package services

import models._

import scala.concurrent.Future

object ClazzService {

  def addClazz(clazz: Clazz): Future[String] = {
    Clazzes.add(clazz)
  }

  def deleteClazz(id: Long): Future[Int] = {
    Clazzes.delete(id)
  }

  def getClazz(id: Long): Future[Option[Clazz]] = {
    Clazzes.get(id)
  }

  def listAllClazzes: Future[Seq[Clazz]] = {
    Clazzes.listAll
  }

  def findAllByIds(classIds: Seq[Long]): Future[Seq[Clazz]] = {
    Clazzes.findAllByIds(classIds)
  }

  def takeOne: Future[Option[Clazz]] = {
    Clazzes.takeOne
  }

}
