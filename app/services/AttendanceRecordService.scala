package services

import models._
import org.joda.time.DateTime

import scala.concurrent.Future

object AttendanceRecordService {
  def addAttendanceRecords(data: Seq[AttendanceRecord]): Future[String] = {
    AttendanceRecords.addAttendanceRecords(data)
  }

  def listAll: Future[Seq[AttendanceRecord]] = {
    AttendanceRecords.listAll
  }

  def newAttendanceRecordsForClass(classId: Long, date: DateTime): Seq[DailyAttendanceRecord] = {
    AttendanceRecords.newAttendanceRecordsForClass(classId, date)
  }

  def getDailyReport: Future[Seq[DailyAttendanceRecord]] = {
    AttendanceRecords.getDailyReport
  }

  def getTermReport: Seq[TermAttendanceRecord] = {
    AttendanceRecords.getTermReport
  }

}
