package models

import java.sql._

import org.joda.time.{DateTime, LocalDate}
import play.api.{Logger, Play}
import play.api.data.Form
import play.api.data.Forms._
import play.api.db.slick.DatabaseConfigProvider
import services.ClazzService
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

case class AttendanceRecord(id: Long, dateTime: Timestamp, present: Boolean, classId: Long, studentId: Long)

case class DailyAttendanceReportData(date: Date, time: Time, present: Boolean, className: String, grade: String, firstName: String, lastName: String)
case class DailyAttendanceRecord(datetime: Timestamp, present: Boolean, clazz: Clazz, student: Student)
case class TermAttendanceRecord(className: String, grade: String, studentName: String, nrAttended: Int, nrMissed: Int)
case class AttendanceRecordFormData(classId: Long, studentId: Long, present: Boolean)
case class AttendanceRecordFormDataList(attendanceRecords: Seq[AttendanceRecordFormData])

object AttendanceRecordForm {
  val form: Form[AttendanceRecordFormDataList] = Form(
    mapping(
      "Attendance Records" -> seq(mapping(
      "Class Id" -> longNumber,
      "Student Id" -> longNumber,
      "Present" -> boolean
    )(AttendanceRecordFormData.apply)(AttendanceRecordFormData.unapply))
  )(AttendanceRecordFormDataList.apply)(AttendanceRecordFormDataList.unapply))
}

class AttendanceRecordTableDef(tag: Tag) extends Table[AttendanceRecord](tag, "attendance_record") {
  def id = column[Long]("id", O.PrimaryKey,O.AutoInc)
  def dateTime = column[Timestamp]("datetime")
  def present = column[Boolean]("present")
  def studentId = column[Long]("student_id")
  def classId = column[Long]("class_id")

  override def * = (id, dateTime, present, classId, studentId) <> (AttendanceRecord.tupled, AttendanceRecord.unapply)

}

object AttendanceRecords {

  val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  val attendanceRecords = TableQuery[AttendanceRecordTableDef]

  def add(attendanceRecord: AttendanceRecord): Future[String] = {
    Logger.info("attendanceRecord =>" + attendanceRecord)
    dbConfig.db.run(attendanceRecords += attendanceRecord).map{res => Logger.info("Attendance record successfully added");"Attendance record successfully added"}.recover {
      case ex: Exception => Logger.error(ex.getCause.getMessage); ex.getCause.getMessage
    }
  }

  def delete(id: Long): Future[Int] = {
    dbConfig.db.run(attendanceRecords.filter(_.id === id).delete)
  }

  def get(id: Long): Future[Option[AttendanceRecord]] = {
    dbConfig.db.run(attendanceRecords.filter(_.id === id).result.headOption)
  }

  def listAll: Future[Seq[AttendanceRecord]] = {
    dbConfig.db.run(attendanceRecords.result)
  }

  def getDailyReport: Future[Seq[DailyAttendanceRecord]] = {
    val start = new Timestamp(LocalDate.now().toDateTimeAtStartOfDay.getMillis)
    val end = new Timestamp(LocalDate.now().plusDays(1).toDateTimeAtStartOfDay.getMillis)

    val joinQuery = for {
      atr <- attendanceRecords
      clazz <- Clazzes.clazzes if atr.classId === clazz.id
      student <- Students.students if atr.studentId === student.id
    } yield (atr.dateTime, atr.present, clazz, student)

    dbConfig.db.run(joinQuery.filter(x => x._1.between(start, end)).result).map(f => f.map(x => new DailyAttendanceRecord(x._1, x._2, x._3, x._4)))

  }

  def getTermReport: Seq[TermAttendanceRecord] = {
    val start = new Timestamp(LocalDate.now().minusMonths(3).toDateTimeAtStartOfDay.getMillis)
    val end = new Timestamp(LocalDate.now().plusDays(1).toDateTimeAtStartOfDay.getMillis)

    val q1 = (for {
      atr <- attendanceRecords.filter(_.dateTime.between(start, end))
      clazz <- Clazzes.clazzes if atr.classId === clazz.id
      student <- Students.students if atr.studentId === student.id
    } yield (clazz, student, atr.present)).groupBy({
      case (clazz, student, present) => (clazz, student, present)
    }).map({
      case ((clazz, student, present), presentCount) => (clazz, student, present, presentCount.map(_._3).length)
    })

    val q2 = q1.groupBy(x => (x._1,x._2)).map{
      case ((clazz, student), res) => (
        clazz,
        student,
        res.filter(_._3).map(f => f._4).sum.get,
        res.filterNot(_._3).map(f => f._4).sum.get
        )
    }

    // classId, studentId, present, count => classId, studentId, daysPresent, daysMissed
     val res = Await.result(dbConfig.db.run(q1.result), 30.seconds)

     res.groupBy(x => (x._1,x._2)).map{
       case ((clazz, student), res) => (
         clazz,
         student,
         res.filter(_._3).map(f => f._4).sum,
         res.filterNot(_._3).map(f => f._4).sum
         )
     }.map {  case (clazz, student, daysPresent, daysMissed) =>
         new TermAttendanceRecord(clazz.className, clazz.grade, student.firstName +" " +student.lastName, daysPresent, daysMissed)
      }.toSeq

  }

  def newAttendanceRecordsForClass(classId: Long, date: DateTime): Seq[DailyAttendanceRecord] = {

    val joinQuery = for {
      clazzStudents <- ClazzStudents.clazzStudents if clazzStudents.classId === classId
      students <- Students.students if clazzStudents.studentId === students.id
    } yield (students)

    val clazzOption = Await.result(ClazzService.getClazz(classId), 30.seconds)

    if(clazzOption.isDefined) {
      val clazz = clazzOption.get
      val students = Await.result(dbConfig.db.run(joinQuery.result), 30.seconds)

      students.map(s => new DailyAttendanceRecord(new Timestamp(date.getMillis), false, clazz, s)).sortBy(f => f.student.lastName)
    }else {
      Seq[DailyAttendanceRecord]()
    }
  }

  def addAttendanceRecords(data: Seq[AttendanceRecord]): Future[String] = {
    Logger.info("addAttendanceRecords: " + data.toString)
    val successStr = s"${data.length} attendance records successfully added"
    dbConfig.db.run(attendanceRecords ++= data).map{res => Logger.info(successStr);successStr}.recover {
      case ex: Exception => Logger.error(ex.getCause.getMessage);ex.getCause.getMessage
    }
  }
}