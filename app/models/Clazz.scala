package models

import play.api.Play
import play.api.data.Form
import play.api.data.Forms._
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

case class Clazz (id: Long, className: String, grade: String)

case class ClazzFormData(className: String, grade: String)

object ClazzForm {

  val form = Form(
    mapping(
      "Class Name" -> nonEmptyText,
      "Grade" -> nonEmptyText
    )(ClazzFormData.apply)(ClazzFormData.unapply)
  )

}

class ClazzTableDef(tag: Tag) extends Table[Clazz](tag, "class") {
  def id = column[Long]("id", O.PrimaryKey,O.AutoInc)
  def className = column[String]("class_name")
  def grade = column[String]("grade")

  override def * = (id, className, grade) <>(Clazz.tupled, Clazz.unapply)

}

object Clazzes {

  val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  val clazzes = TableQuery[ClazzTableDef]

  def add(clazz: Clazz): Future[String] = {
    dbConfig.db.run(clazzes += clazz).map(res => "Clazz successfully added").recover {
      case ex: Exception => ex.getCause.getMessage
    }
  }

  def delete(id: Long): Future[Int] = {
    dbConfig.db.run(clazzes.filter(_.id === id).delete).recover {
      case ex: Exception => 0
    }
  }

  def get(id: Long): Future[Option[Clazz]] = {
    dbConfig.db.run(clazzes.filter(_.id === id).result.headOption)
  }

  def listAll: Future[Seq[Clazz]] = {
    dbConfig.db.run(clazzes.result)
  }

  def findAllByIds(classIds: Seq[Long]): Future[Seq[Clazz]] = {
    dbConfig.db.run(
      clazzes.filter(x => x.id inSet classIds).result
    )
  }

  def takeOne : Future[Option[Clazz]] = {
    dbConfig.db.run(
      clazzes.result.headOption
    )
  }

}