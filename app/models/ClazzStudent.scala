package models

import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import services.StudentService
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

case class ClazzStudent (id: Long, classId: Long, studentId: Long)

class ClazzStudentTableDef(tag: Tag) extends Table[ClazzStudent](tag, "class_student") {
  def id = column[Long]("id", O.PrimaryKey,O.AutoInc)
  def classId = column[Long]("class_id")
  def studentId = column[Long]("student_id")

  override def * = (id, classId, studentId) <>(ClazzStudent.tupled, ClazzStudent.unapply)

}

object ClazzStudents {

  val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  val clazzStudents = TableQuery[ClazzStudentTableDef]

  def add(clazzStudent: ClazzStudent): Future[String] = {
    dbConfig.db.run(clazzStudents += clazzStudent).map(res => "ClazzStudent successfully added").recover {
      case ex: Exception => ex.getCause.getMessage
    }
  }

  def listAll: Future[Seq[ClazzStudent]] = {
    dbConfig.db.run(clazzStudents.result)
  }

  def listStudentsForClazz(classId: Long): Future[Seq[Student]] = {

    val joinQuery = for {
      cs <- clazzStudents if cs.classId === classId
      student <- Students.students if cs.studentId === student.id
    } yield student

    dbConfig.db.run(joinQuery.result)

  }

  def addStudent(classId: Long, newStudent: Student): Student = {
    val student = Await.result(StudentService.addStudent(newStudent), 30.seconds)

    val newClazzStudent = new ClazzStudent(0, classId, student.id)
    add(newClazzStudent)

    student
  }

}





