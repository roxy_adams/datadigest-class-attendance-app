package models

import play.api.Play
import play.api.data.Form
import play.api.data.Forms._
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile
import slick.driver.PostgresDriver.api._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

case class Student (id: Long, firstName: String, lastName: String)

case class StudentFormData(firstName: String, lastName: String, present: Boolean)

object StudentForm {

  val form = Form(
    mapping(
      "First Name" -> nonEmptyText,
      "Last Name" -> nonEmptyText,
      "Present" -> boolean
    )(StudentFormData.apply)(StudentFormData.unapply)
  )

}

class StudentTableDef(tag: Tag) extends Table[Student](tag, "student") {
  def id = column[Long]("id", O.PrimaryKey,O.AutoInc)
  def firstName = column[String]("first_name")
  def lastName = column[String]("last_name")

  override def * = (id, firstName, lastName) <>(Student.tupled, Student.unapply)

}

object Students {

  val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  val students = TableQuery[StudentTableDef]
  val insertQuery = students returning students.map(_.id) into ((student, id) => student.copy(id = id))

  def add(student: Student): Future[Student] = {
    dbConfig.db.run(insertQuery += student)
  }

  def delete(id: Long): Future[Int] = {
    dbConfig.db.run(students.filter(_.id === id).delete)
  }

  def get(id: Long): Future[Option[Student]] = {
    dbConfig.db.run(students.filter(_.id === id).result.headOption)
  }
  
  def listAll: Future[Seq[Student]] = {
    dbConfig.db.run(students.result)
  }

  def findAllByIds(studentIds: Seq[Long]): Future[Seq[Student]] = {
    dbConfig.db.run(
      students.filter(x => x.id inSet studentIds).result
    )
  }

}





