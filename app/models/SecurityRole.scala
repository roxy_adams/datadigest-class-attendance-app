package models

sealed trait SecurityRole

object SecurityRole {

  case object Administrator extends SecurityRole

  case object NormalUser extends SecurityRole

  def valueOf(value: String): SecurityRole = value match {
    case "Administrator" => Administrator
    case "NormalUser" => NormalUser
    case _ => throw new IllegalArgumentException()
  }
}