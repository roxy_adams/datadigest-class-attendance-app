package controllers


import play.api.mvc._
import play.api.i18n.Messages.Implicits._
import play.api.Play.current

import controllers.security.AuthConfigImpl
import jp.t2v.lab.play2.auth.AuthElement
import models.SecurityRole.NormalUser
import services._
import models._
import play.twirl.api.Html
import views._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await}
import scala.concurrent.duration._

class Application extends Controller with AuthElement with AuthConfigImpl {

  def index = StackAction(AuthorityKey -> NormalUser) { implicit request => {
    Ok(html.index("Hello")(new Html("")))
  }
  }

  def classes = AsyncStack(AuthorityKey -> NormalUser) { implicit request =>
    ClazzService.listAllClazzes.map { clazzes =>
      Ok(html.class_admin(ClazzForm.form, clazzes))
    }
  }

  def students = StackAction(AuthorityKey -> NormalUser) { implicit request => {
    val clazz = Await.result(ClazzService.takeOne, 30.seconds)

    if (clazz.isDefined) {
      Redirect(routes.ClassAdmin.adminForClass(clazz.get.id))
    } else {
      Redirect(routes.Application.classes())
    }
  }
  }

  def attendance = StackAction(AuthorityKey -> NormalUser) { implicit request => {
    val clazz = Await.result(ClazzService.takeOne, 30.seconds)

    if (clazz.isDefined) {
      Redirect(routes.ClassAdmin.attendanceForClass(clazz.get.id))
    } else {
      Redirect(routes.Application.classes())
    }
  }
  }

}
