package controllers

import controllers.security.AuthConfigImpl
import jp.t2v.lab.play2.auth.{AuthElement, LoginLogout}
import models.SecurityRole.{Administrator, NormalUser}
import models._
import play.api.mvc._
import play.api.Play.current
import play.api.i18n.Messages.Implicits._

import scala.concurrent.Future
import services.AccountsService

import scala.concurrent.ExecutionContext.Implicits.global

class SecurityController extends Controller with AuthElement with AuthConfigImpl {
  def account = StackAction(AuthorityKey -> NormalUser) { implicit request =>
    Ok(views.html.accounts(AccountForm.form))
  }

  def addAccount() = AsyncStack(AuthorityKey -> Administrator) { implicit request =>
    AccountForm.form.bindFromRequest.fold(
      formWithErrors => Future.successful(BadRequest(views.html.accounts(formWithErrors))),
      data => {
        val newAccount = Account(0, data.email, data.password, data.name, data.role)
        AccountsService.addUser(newAccount).map(res =>
          Redirect(routes.SecurityController.account())
        )
      })
  }
}
