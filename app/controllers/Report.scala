package controllers

import controllers.security.AuthConfigImpl
import jp.t2v.lab.play2.auth.AuthElement
import models.SecurityRole.NormalUser
import play.api.mvc._
import services._
import views._

import scala.concurrent.ExecutionContext.Implicits.global

class Report extends Controller with AuthElement with AuthConfigImpl {

  def dailyReport = AsyncStack(AuthorityKey -> NormalUser) { implicit request =>
    AttendanceRecordService.getDailyReport map { attendanceRecords =>
      Ok(html.reports.daily_report(attendanceRecords.sortBy(f => (f.clazz.className, f.clazz.grade, f.student.firstName, f.student.lastName))))
    }
  }

  def termReport = StackAction(AuthorityKey -> NormalUser) { implicit request =>
      Ok(html.reports.term_report(AttendanceRecordService.getTermReport.sortBy(f => (f.className, f.grade, f.studentName))))
  }

}
