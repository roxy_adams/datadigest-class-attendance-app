package controllers

import play.api.mvc.{Action, Controller}
import play.api.i18n.Messages.Implicits._
import play.api.Play.current
import play.api.Logger
import javax.inject.Inject
import java.sql._

import controllers.security.AuthConfigImpl
import jp.t2v.lab.play2.auth.AuthElement
import models.SecurityRole.NormalUser
import models._
import org.joda.time.{DateTime, LocalDate}
import services.{AttendanceRecordService, ClazzService, ClazzStudentService}
import views.html

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

class ClassAdmin extends Controller with AuthElement with AuthConfigImpl {

  def allClasses = Await.result(ClazzService.listAllClazzes, 30.seconds).sortBy(f => (f.className, f.grade))
  def studentsForClass(classId: Long) = Await.result(ClazzStudentService.listStudentsForClazz(classId), 30.seconds).sortBy(f => (f.firstName, f.lastName))

  def admin = StackAction(AuthorityKey -> NormalUser) { implicit request => {
    val clazz = Await.result(
      ClazzService.takeOne, 30.seconds
    )

    if (clazz.isDefined) {
      Redirect(routes.ClassAdmin.adminForClass(clazz.get.id))
    } else {
      Redirect(routes.Application.index())
    }
  }
  }

  def adminForClass(selectedClassId: Long) = StackAction(AuthorityKey -> NormalUser) { implicit request => {
    Ok(html.student_admin(StudentForm.form, selectedClassId, studentsForClass(selectedClassId), allClasses))
  }
  }

  def createClazz() = AsyncStack(AuthorityKey -> NormalUser) { implicit request =>
    ClazzForm.form.bindFromRequest.fold(
      // if any error in submitted data
      errorForm => Future.successful(BadRequest(html.class_admin(errorForm, allClasses))),
      data => {
        val newClazz = Clazz(0, data.className, data.grade)
        ClazzService.addClazz(newClazz).map(res =>
          Redirect(routes.Application.classes())
        )
      })
  }

  def deleteClazz(id: Long) = AsyncStack(AuthorityKey -> NormalUser) { implicit request =>
    //if exist in class_students - error
    ClazzService.deleteClazz(id) map { res =>
      Redirect(routes.Application.classes())
    }
  }

  def createStudent(classId: Long) = StackAction(AuthorityKey -> NormalUser) { implicit request =>
    StudentForm.form.bindFromRequest.fold(
      // if any error in submitted data
      errorForm => BadRequest(html.student_admin(errorForm, classId, studentsForClass(classId), allClasses)),
      data => {
        val newStudent = Student(0, data.firstName, data.lastName)
        ClazzStudentService.addStudent(classId, newStudent)

        Redirect(routes.ClassAdmin.adminForClass(classId))
      })
  }


  def submitAttendance(selectedClassId: Long) = StackAction(AuthorityKey -> NormalUser) { implicit request =>
    AttendanceRecordForm.form.bindFromRequest.fold(
      // if any error in submitted data
      errorForm => {
        Logger.error("submitAttendance error")
        BadRequest(html.attendance(errorForm, studentsForClass(selectedClassId), selectedClassId, allClasses))
      },
      data => {
        val dateTime = new Timestamp(DateTime.now().getMillis)
        val result = AttendanceRecordService.addAttendanceRecords(
          data.attendanceRecords.map(x => new AttendanceRecord(0, dateTime, x.present, x.classId, x.studentId))
        )
        Redirect(routes.Report.dailyReport)
      })
  }

  def attendanceForClass(classId: Long) = StackAction(AuthorityKey -> NormalUser) { implicit request =>
    val newRecords = AttendanceRecordService.newAttendanceRecordsForClass(classId, LocalDate.now().toDateTimeAtCurrentTime)
    val list = newRecords.map(nr => new AttendanceRecordFormData(nr.clazz.id, nr.student.id, nr.present))

    Ok(html.attendance(AttendanceRecordForm.form.fill(new AttendanceRecordFormDataList(list)), studentsForClass(classId), classId, allClasses))

  }
}
