import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.test._
import play.api.test.Helpers._

import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
@RunWith(classOf[JUnitRunner])
class ApplicationSpec extends Specification with InMemoryDB {

  "Application" should {

    "send 404 on a bad request" in new WithApplication(app = appWithMemoryDatabase) {
      val error = Await.result(route(FakeRequest(GET, "/blah")).get, 10 seconds)
      error.header.status shouldEqual NOT_FOUND
    }

    "401 the index page without auth" in new WithApplication(app = appWithMemoryDatabase) {
      val home = route(FakeRequest(GET, "/")).get

      status(home) must equalTo(UNAUTHORIZED)
    }
  }
}
