INSERT INTO class (class_name, grade) VALUES ('English', '8A');
INSERT INTO class (class_name, grade) VALUES ('English', '9A');
INSERT INTO class (class_name, grade) VALUES ('English', '9B');
INSERT INTO class (class_name, grade) VALUES ('English', '10');

INSERT INTO student (first_name, last_name) VALUES ('Finn', 'The Human');
INSERT INTO student (first_name, last_name) VALUES ('Jake', 'The Dog');
INSERT INTO student (first_name, last_name) VALUES ('Susan', 'Strong');
INSERT INTO student (first_name, last_name) VALUES ('Lady', 'Rainicorn');
INSERT INTO student (first_name, last_name) VALUES ('Tree', 'Trunks');
INSERT INTO student (first_name, last_name) VALUES ('Peppermint', 'Butler');
INSERT INTO student (first_name, last_name) VALUES ('Cinnamon', 'Bun');
INSERT INTO student (first_name, last_name) VALUES ('Marceline', 'The Vampire Queen');
INSERT INTO student (first_name, last_name) VALUES ('Ice', 'King');
INSERT INTO student (first_name, last_name) VALUES ('B', 'MO');
INSERT INTO student (first_name, last_name) VALUES ('Magic', 'Man');
INSERT INTO student (first_name, last_name) VALUES ('Bubblegum', 'Princess');
INSERT INTO student (first_name, last_name) VALUES ('Hunson', 'Abadeer');
INSERT INTO student (first_name, last_name) VALUES ('Flame', 'Princess');
INSERT INTO student (first_name, last_name) VALUES ('Lumpy Space', 'Princess');
INSERT INTO student (first_name, last_name) VALUES ('Slime', 'Princess');
INSERT INTO student (first_name, last_name) VALUES ('Hotdog', 'Princess');
INSERT INTO student (first_name, last_name) VALUES ('Docter', 'Princess');
INSERT INTO student (first_name, last_name) VALUES ('Breakfast', 'Princess');

INSERT INTO class_student (class_id, student_id) VALUES (1, 1)
INSERT INTO class_student (class_id, student_id) VALUES (2, 2)
INSERT INTO class_student (class_id, student_id) VALUES (3, 3)
INSERT INTO class_student (class_id, student_id) VALUES (4, 4)
INSERT INTO class_student (class_id, student_id) VALUES (1, 5)
INSERT INTO class_student (class_id, student_id) VALUES (2, 6)
INSERT INTO class_student (class_id, student_id) VALUES (3, 7)
INSERT INTO class_student (class_id, student_id) VALUES (4, 8)
INSERT INTO class_student (class_id, student_id) VALUES (1, 9)
INSERT INTO class_student (class_id, student_id) VALUES (2, 10)
INSERT INTO class_student (class_id, student_id) VALUES (3, 11)
INSERT INTO class_student (class_id, student_id) VALUES (4, 12)
INSERT INTO class_student (class_id, student_id) VALUES (1, 13)
INSERT INTO class_student (class_id, student_id) VALUES (2, 14)
INSERT INTO class_student (class_id, student_id) VALUES (3, 15)
INSERT INTO class_student (class_id, student_id) VALUES (4, 16)
INSERT INTO class_student (class_id, student_id) VALUES (1, 17)
INSERT INTO class_student (class_id, student_id) VALUES (2, 18)
INSERT INTO class_student (class_id, student_id) VALUES (3, 19)